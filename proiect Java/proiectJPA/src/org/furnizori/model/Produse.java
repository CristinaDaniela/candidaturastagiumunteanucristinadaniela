package org.furnizori.model;
import java.util.ArrayList;
import java.util.List;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.GeneratedValue;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import static javax.persistence.GenerationType.SEQUENCE;


@SequenceGenerator(name="seq", initialValue=100, allocationSize=1)
@Entity
public class Produse {
	// Atribute private
	
	@GeneratedValue(strategy= SEQUENCE, generator = "seq")
	@Id
	@Column( name = "cod" )
	private Integer cod;
	private String denumire;
	private String um;
	private Double pretUnitar;
	@OneToMany(mappedBy = "produs", cascade=CascadeType.PERSIST)
	private List<ObiecteContract>obiecte=new ArrayList<ObiecteContract>();

	// Constructori

	public List<ObiecteContract> getObiecte() {
		return obiecte;
	}



	public void setObiecte(List<ObiecteContract> obiecte) {
		this.obiecte = obiecte;
	}



	public Produse(String denumire, String um, Double pretUnitar) {
		super();
		this.denumire = denumire;
		this.um = um;
		this.pretUnitar = pretUnitar;
	}



	public Produse(Integer cod, String denumire, String um, Double pretUnitar) {
		super();
		this.cod = cod;
		this.denumire = denumire;
		this.um = um;
		this.pretUnitar = pretUnitar;
		//this.stoc = stoc;
	}

	

	public Produse(){}
	
	// Getteri si setteri
	
	public Integer getCod() {
		return cod;
	}

	public void setCod(Integer cod) {
		this.cod = cod;
	}

	public String getDenumire() {
		return denumire;
	}

	public void setDenumire(String denumire) {
		this.denumire = denumire;
	}

	public String getUm() {
		return um;
	}

	public void setUm(String um) {
		this.um = um;
	}

	public Double getPretUnitar() {
		return pretUnitar;
	}

	public void setPretUnitar(Double pretUnitar) {
		this.pretUnitar = pretUnitar;
	}

	// Criteriu de egalitate
	
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Produse other = (Produse) obj;
		if (cod == null) {
			if (other.cod != null)
				return false;
		} else if (!cod.equals(other.cod))
			return false;
		return true;
	}	

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((cod == null) ? 0 : cod.hashCode());
		return result;
	}	
	
	// Operatii specifice logicii modelului afacerii	
	
	public String toString(){
		return "Produs: cod:" + this.cod + ", denumire:"  + this.denumire;
	}	
}