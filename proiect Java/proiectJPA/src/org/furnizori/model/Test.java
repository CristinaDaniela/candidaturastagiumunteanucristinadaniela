package org.furnizori.model;


import java.util.Calendar;
import java.util.List;
import java.util.ArrayList;
import javax.persistence.*;

public class Test {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		
	
		EntityManagerFactory emf=Persistence.createEntityManagerFactory("proiectJPA");
		EntityManager em=emf.createEntityManager();
	 	
		List<ObiecteContract>lstObiecte=em.createQuery("SELECT o FROM ObiecteContract o").getResultList();
		if(!lstObiecte.isEmpty())
		{
			em.getTransaction().begin();
			for(ObiecteContract o:lstObiecte)
				em.remove(o);
			em.getTransaction().commit();
		}
		List<ContracteAprovizionare>lstContracte=em.createQuery("SELECT c FROM ContracteAprovizionare c").getResultList();
		if(!lstContracte.isEmpty())
		{
			em.getTransaction().begin();
			for(ContracteAprovizionare c:lstContracte)
				em.remove(c);
			em.getTransaction().commit();
		}
		List<FurnizorIntern>lstFurnizoriI=em.createQuery("SELECT fi FROM FurnizorIntern fi").getResultList();
		if(!lstFurnizoriI.isEmpty())
		{
			em.getTransaction().begin();
			for(FurnizorIntern fi:lstFurnizoriI)
				em.remove(fi);
			em.getTransaction().commit();
		}
		List<FurnizorExtern>lstFurnizoriE=em.createQuery("SELECT fe FROM FurnizorExtern fe").getResultList();
		if(!lstFurnizoriE.isEmpty())
		{
			em.getTransaction().begin();
			for(FurnizorExtern fe:lstFurnizoriE)
				em.remove(fe);
			em.getTransaction().commit();
		}
		List<Stocuri>lstStocuri=em.createQuery("SELECT s FROM Stocuri s").getResultList();
		if(!lstStocuri.isEmpty())
		{
			em.getTransaction().begin();
			for(Stocuri s:lstStocuri)
				em.remove(s);
			em.getTransaction().commit();
		}
		List<Produse>lstProduse=em.createQuery("SELECT p FROM Produse p").getResultList();
		if(!lstProduse.isEmpty())
		{
			em.getTransaction().begin();
			for(Produse p:lstProduse)
				em.remove(p);
			em.getTransaction().commit();
		}
		
		Calendar c11 = Calendar.getInstance();
		c11.set(Calendar.YEAR, 2014);
		c11.set(Calendar.DAY_OF_MONTH, 13);
		c11.set(Calendar.MONTH, 11); 
		Calendar c12 = Calendar.getInstance();
		c12.set(Calendar.YEAR, 2015);
		c12.set(Calendar.DAY_OF_MONTH, 30);
		c12.set(Calendar.MONTH, 1); 
		Calendar c13 = Calendar.getInstance();
		c13.set(Calendar.YEAR, 2015);
		c13.set(Calendar.DAY_OF_MONTH, 13);
		c13.set(Calendar.MONTH, 2); 
		Calendar c14 = Calendar.getInstance();
		c14.set(Calendar.YEAR, 2015);
		c14.set(Calendar.DAY_OF_MONTH, 27);
		c14.set(Calendar.MONTH, 2); 
		java.sql.Date d1 = new java.sql.Date(c11.getTime().getTime());
		java.sql.Date d2 = new java.sql.Date(c12.getTime().getTime());
		java.sql.Date d3 = new java.sql.Date(c13.getTime().getTime());
		java.sql.Date d4 = new java.sql.Date(c14.getTime().getTime());

	
		List<Stocuri> stocuri =new ArrayList<Stocuri>();
		Stocuri s1=new Stocuri(1000.0,d1,"");
		Stocuri s2=new Stocuri(1879.0,d3,"");
		Stocuri s3=new Stocuri(289.9,d2,"");
		Stocuri s4=new Stocuri(76409.9,d1,"");
		Stocuri s5=new Stocuri(1228.9,d1,"");
		Stocuri s6=new Stocuri(2139.1,d1,"");
		Stocuri s7=new Stocuri(3133.2,d1,"");

		s1.setId(s1.getId());
		s2.setId(s2.getId());
		s3.setId(s3.getId());
		s4.setId(s4.getId());
		s5.setId(s5.getId());
		s6.setId(s6.getId());
		s7.setId(s7.getId());
		
		List<Produse> produse =new ArrayList<Produse>();
		Produse p1=new Produse("Biscuiti Oreo", "pachet", 2.0);
		Produse p2=new Produse("Detergent Ariel", "kg", 9.0);
		Produse p3=new Produse("Ciocolata Milka", "buc", 3.0);
		Produse p4=new Produse("Biscuiti Poieni", "pachet", 1.5);
		Produse p5=new Produse("Zahar Coronita", "kg", 3.0);
		Produse p6=new Produse("Ciocolata Katy", "buc", 1.5);
		Produse p7=new Produse("Bomboane Kandia", "kg", 20.0);
		p1.setCod(p1.getCod());
		p2.setCod(p2.getCod());
		p3.setCod(p3.getCod());
		p4.setCod(p4.getCod());
		p5.setCod(p5.getCod());
		p6.setCod(p6.getCod());
		p7.setCod(p7.getCod());
		
		s1.setProdus(p1);	
		s2.setProdus(p2);	
		s3.setProdus(p3);	
		s4.setProdus(p4);	
		s5.setProdus(p5);	
		s6.setProdus(p6);	
		s7.setProdus(p7);	
	
		produse.add(p1);
		produse.add(p2);
		produse.add(p3);
		produse.add(p4);
		produse.add(p5);
		produse.add(p6);
		produse.add(p7);

		stocuri.add(s1);
		stocuri.add(s2);
		stocuri.add(s3);
		stocuri.add(s4);
		stocuri.add(s5);
		stocuri.add(s6);
		stocuri.add(s7);
	         
	      
		List<FurnizorIntern> furnizoriI =new ArrayList<FurnizorIntern>();
		List<FurnizorExtern> furnizoriE =new ArrayList<FurnizorExtern>();

		FurnizorExtern f1= new FurnizorExtern(101, "Axta SRL","Str. M. Eminescu, nr.39, Iasi","0756654570", "extern","Germania", "F009811");
		FurnizorExtern f2= new FurnizorExtern(102, "P&G","Str.Erekj, nr.39,Berlin, Germania","0093356654570","extern","Islanda", "G1128980" );
		FurnizorIntern f3=new FurnizorIntern(103, "Beta SRL","Str. N. Iorga, nr.100, Bucuresti","0756994570","intern", "123456");
		FurnizorIntern f4=new FurnizorIntern(104, "Inter SRL","Str. Carol I, nr.123, Iasi","0756999998","intern", "123116");
		furnizoriE.add(f1);
		furnizoriE.add(f2);
		furnizoriI.add(f3);
		furnizoriI.add(f4);
		List<Furnizor> furnizori =new ArrayList<Furnizor>();
		furnizori.add(f1);
		furnizori.add(f2);
		furnizori.add(f3);
		furnizori.add(f4);

		
		List<ContracteAprovizionare> ca =new ArrayList<ContracteAprovizionare>();
		ContracteAprovizionare c1=new ContracteAprovizionare(1000,d1,f1);
		ContracteAprovizionare c2=new ContracteAprovizionare(1001,d2,f2);
		ContracteAprovizionare c3=new ContracteAprovizionare(1002,d3,f1);
		ContracteAprovizionare c4=new ContracteAprovizionare(1003,d4,f3);
		ContracteAprovizionare c5=new ContracteAprovizionare(1004,d4,f1);
		ca.add(c1);
		ca.add(c2);
		ca.add(c3);
		ca.add(c4);
		ca.add(c5);

	
		List<ObiecteContract> oc =new ArrayList<ObiecteContract>();
		oc.add(new ObiecteContract(1,p1,1300.0,c1));
		oc.add(new ObiecteContract(2,p3,1000.0,c1));
		oc.add(new ObiecteContract(3,p7,45600.0,c1));
		oc.add(new ObiecteContract(4,p4,25000.0,c2));
		oc.add(new ObiecteContract(5,p1,1300.0,c2));
		oc.add(new ObiecteContract(6,p3,6780.0,c2));
		oc.add(new ObiecteContract(7,p5,8990.0,c3));
		oc.add(new ObiecteContract(8,p6,1130.0,c3));
		oc.add(new ObiecteContract(9,p7,2340.0,c4));
		oc.add(new ObiecteContract(10,p1,67800.0,c5));
		oc.add(new ObiecteContract(11,p3,3240.0,c5));		
		
	

		em.getTransaction().begin();
		em.persist(produse.get(0));
		em.persist(produse.get(1));
		em.persist(produse.get(2));
		em.persist(produse.get(3));
		em.persist(produse.get(4));
		em.persist(produse.get(5));
		em.persist(produse.get(6));
		em.getTransaction().commit();
		List<Produse>lstProdusePersistente=em.createQuery("SELECT p FROM Produse p").getResultList();
		System.out.println("Lista produselor persistente/salvate in baza de date!!!");
		for(Produse p: lstProdusePersistente)
			System.out.println("Cod: "+p.getCod()+ ", denumire: "+ p.getDenumire()+", u.m.: "+p.getUm()+", pret unitar: "+p.getPretUnitar());
		
		em.getTransaction().begin();
		em.persist(stocuri.get(0));
		em.persist(stocuri.get(1));
		em.persist(stocuri.get(2));
		em.persist(stocuri.get(3));
		em.persist(stocuri.get(4));
		em.persist(stocuri.get(5));
		em.persist(stocuri.get(6));
		em.getTransaction().commit();
		List<Stocuri>lstStocuriPersistente=em.createQuery("SELECT s FROM Stocuri s").getResultList();
		System.out.println("Lista stocurilorlor persistente/salvate in baza de date!!!");
		for(Stocuri s: lstStocuriPersistente)
			System.out.println("Id:  "+s.getId()+ ", produs:  "+ ", stoc:  "+s.getStoc()+", data actualizare:  "+s.getDataActualizare());
		em.getTransaction().begin();
		em.persist(furnizori.get(0));
		em.persist(furnizori.get(1));
		em.persist(furnizori.get(2));
		em.persist(furnizori.get(3));
		em.getTransaction().commit();
    	List<Furnizor> lstFurnizoriPersistenti=em.createQuery("SELECT f FROM Furnizor f").getResultList();
		System.out.println("Lista furnizorilor persistenti/salvati in baza de date");
		for(Furnizor f: lstFurnizoriPersistenti)
			System.out.println("Id: "+f.getId()+ ", nume: "+ f.getNume());
		
		em.getTransaction().begin();
		em.persist(oc.get(0));
		em.persist(oc.get(1));
		em.persist(oc.get(2));
		em.persist(oc.get(3));
		em.persist(oc.get(4));
		em.persist(oc.get(5));
		em.persist(oc.get(6));
		em.persist(oc.get(7));
		em.persist(oc.get(8));
		em.persist(oc.get(9));
		em.persist(oc.get(10));
		em.getTransaction().commit();
		List<ObiecteContract>lstObiectePersistente=em.createQuery("SELECT o FROM ObiecteContract o").getResultList();
		System.out.println("Lista obiectelor persistente/salvate in baza de date!!!");
		for(ObiecteContract o: lstObiectePersistente)
		System.out.println("Id: "+o.getId()+ ", produs: "+ o.getProdus()+", contract: "+o.getContract());
		
		
		em.getTransaction().begin();
		em.persist(ca.get(0));
		em.persist(ca.get(1));
		em.persist(ca.get(2));
		em.persist(ca.get(3));
		em.persist(ca.get(4));
		em.getTransaction().commit();
		List<ContracteAprovizionare>lstContractePersistente=em.createQuery("SELECT c FROM ContracteAprovizionare c").getResultList();
		System.out.println("Lista contractelor persistente/salvate in baza de date!!!");
		for(ContracteAprovizionare c: lstContractePersistente)
			System.out.println("Id: "+c.getId()+ ", data: "+ c.getDataContract()+", furnizor: "+c.getFurnizor());
		
		
	
	
	
		
	
	}

}
