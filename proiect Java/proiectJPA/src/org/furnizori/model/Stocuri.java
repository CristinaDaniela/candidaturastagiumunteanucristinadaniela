package org.furnizori.model;

import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.GeneratedValue;
import javax.persistence.OneToOne;
import javax.persistence.PrimaryKeyJoinColumn;
import javax.persistence.SequenceGenerator;
import javax.persistence.Temporal;

import static javax.persistence.TemporalType.DATE;
import static javax.persistence.GenerationType.SEQUENCE;

@SequenceGenerator(name="sq", initialValue=100, allocationSize=1)
@Entity 
public class Stocuri {
	
	//@GeneratedValue(strategy = AUTO)
	@GeneratedValue(strategy= SEQUENCE, generator = "sq")
	@Id
	private Integer id;
	private Double stoc;
	@Temporal(DATE)
	private Date dataActualizare;
	private String obs;
	@OneToOne
	@PrimaryKeyJoinColumn
	private Produse produs;
	public Produse getProdus() {
		return produs;
	}
	public void setProdus(Produse produs) {
		this.produs = produs;
	}
	public Stocuri(Integer id, Double stoc, Date dataActualizare, String obs) {
		super();
		this.id = id;
		this.stoc = stoc;
		this.dataActualizare = dataActualizare;
		this.obs = obs;
	}
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	
	public Double getStoc() {
		return stoc;
	}
	public void setStoc(Double stoc) {
		this.stoc = stoc;
	}
	public Date getDataActualizare() {
		return dataActualizare;
	}
	public void setDataActualizare(Date dataActualizare) {
		this.dataActualizare = dataActualizare;
	}
	public String getObs() {
		return obs;
	}
	public void setObs(String obs) {
		this.obs = obs;
	}

	public Stocuri() {
		super();
	}
	public Stocuri(Double stoc, Date dataActualizare, String obs) {
		super();
		this.stoc = stoc;
		this.dataActualizare = dataActualizare;
		this.obs = obs;
	}
	

}
