package org.furnizori.model;

import javax.persistence.Entity;


@Entity
public class FurnizorIntern extends Furnizor {
	private String  codFiscal;
	public String getCodFiscal() {
		return codFiscal;
	}

	public void setCodFiscal(String codFiscal) {
		this.codFiscal = codFiscal;
	}
	public FurnizorIntern() {
		super();
	}

	public FurnizorIntern(Integer id, String nume, String adresa,
			String telefon,String tip, String codFiscal) {
		super(id, nume, adresa, telefon,tip);
		this.codFiscal = codFiscal;
	}

	
	


}
