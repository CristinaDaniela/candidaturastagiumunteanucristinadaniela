package org.furnizori.model;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.GeneratedValue;

import static javax.persistence.GenerationType.AUTO;

@Entity
public class ObiecteContract {
	// Atribute private
	@Id
	@GeneratedValue(strategy = AUTO)
	private Integer id;
	
	@ManyToOne
	private Produse produs;
	
	private Double cantitate;
	
	@ManyToOne(cascade=CascadeType.PERSIST)
	private ContracteAprovizionare contract;
	
	// Constructori

	public ObiecteContract(){}
	
	public ObiecteContract(Integer id, Produse produs, Double cantitate,
			ContracteAprovizionare contract) {
		super();
		this.id = id;
		this.produs = produs;
		this.cantitate = cantitate;
		this.contract = contract;
	}

	// Getteri si setteri

	
	public ObiecteContract(Integer id, Produse produs, Double cantitate) {
		this.id = id;
		this.produs = produs;
		this.cantitate = cantitate;
	}

	public ContracteAprovizionare getContract() {
		return contract;
	}

	public void setContract(ContracteAprovizionare contract) {
		this.contract = contract;
	}

	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public Produse getProdus() {
		return produs;
	}
	public void setProdus(Produse produs) {
		this.produs = produs;
	}
	public Double getCantitate() {
		return cantitate;
	}
	public void setCantitate(Double cantitate) {
		this.cantitate = cantitate;
	}
	public Double getValoareObiect(){
		if (produs == null || cantitate == null)
			return 0.0;
		else
		return produs.getPretUnitar() * cantitate;
	}

	// Criteriu de egalitate
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		ObiecteContract other = (ObiecteContract) obj;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		return true;
	}
	
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		return result;
	}
	
	// Operatii specifice logicii modelului afacerii
	
	@Override
	public String toString() {
		return "ArticolComanda: id:" + id + ", " + produs
				+ ", cantitate:" + cantitate + ", valoare Articol:"
				+ getValoareObiect();
	}	
	
}
