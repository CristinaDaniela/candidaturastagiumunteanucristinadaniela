package org.furnizori.model;

import javax.persistence.Entity;


@Entity
public class FurnizorExtern extends Furnizor{
	private String tara;
	private String codFiscal;
	public String getTara() {
		return tara;
	}
	public void setTara(String tara) {
		this.tara = tara;
	}

	public FurnizorExtern() {
		super();
	}
	public FurnizorExtern(Integer id, String nume, String adresa,
			String telefon,String tip, String tara, String codFiscal) {
		super(id, nume, adresa, telefon,tip);
		this.tara = tara;
		this.codFiscal = codFiscal;
	}
	public String getCodFiscal() {
		return codFiscal;
	}
	public void setCodFiscal(String codFiscal) {
		this.codFiscal = codFiscal;
	}

	
	 
	

}
