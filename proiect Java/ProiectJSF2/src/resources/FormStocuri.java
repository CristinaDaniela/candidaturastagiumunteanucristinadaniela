package resources;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

import org.furnizori.model.*;

public class FormStocuri {
	private Stocuri stoc;
	private List<Stocuri> stocuri = new ArrayList<Stocuri>();
	
	public Stocuri getStoc() {
		return stoc;
	}
	public void setStoc(Stocuri stoc) {
		this.stoc = stoc;
	}
	public List<Stocuri> getStocuri() {
		return stocuri;
	}
	public void setStocuri(List<Stocuri> stocuri) {
		this.stocuri = stocuri;
	}
	
	public Integer getIdStoc()
	{
		return this.stoc.getId();
	}
	
	public void setIdStoc(Integer id)
	{
		Integer idx=this.stocuri.indexOf(new Stocuri(id,null,null,""));
		this.stoc=this.stocuri.get(idx);
	}
	
	
	private EntityManager em; 

	public FormStocuri() {
		EntityManagerFactory emf = Persistence.createEntityManagerFactory("proiectJPA");
		em = emf.createEntityManager();
		this.stocuri = em.createQuery("SELECT s FROM Stocuri s").getResultList();
		
		System.out.println("stocuri.count: " + this.stocuri.size());
		
		if (!this.stocuri.isEmpty())
			stoc = this.stocuri.get(0);
	}

	public String adaugare(){
		this.stoc = new Stocuri();
		this.stocuri.add(stoc);
		return "";
	}
	
	public String delete(Stocuri std){
		this.stocuri.remove(std);
		return "";
	}
}
