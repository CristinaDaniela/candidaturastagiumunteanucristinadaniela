package forms;

import java.util.ArrayList;
import java.util.List;

import javax.faces.event.ActionEvent;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

import org.furnizori.model.*;


public class FormFurnizori {
	private FurnizorIntern furnizor;
	private List<FurnizorIntern> furnizori = new ArrayList<FurnizorIntern>();
	public List<FurnizorIntern> getFurnizoriInterni() {
		return furnizori;
	}
	
	public Integer getIdFurnizor()
	{
		return this.furnizor.getId();
	}
	
	public void setIdFurnizor(Integer id)
	{
		Integer idx=this.furnizori.indexOf(new FurnizorIntern(id,"","","","",""));
		this.furnizor=this.furnizori.get(idx);
	}
	
	private EntityManager em; 

	public FormFurnizori() {
		EntityManagerFactory emf = Persistence.createEntityManagerFactory("proiectJPA");
		em = emf.createEntityManager();
		this.furnizori = em.createQuery("SELECT f FROM FurnizorIntern f").getResultList();		
		if (!this.furnizori.isEmpty())
			furnizor = this.furnizori.get(0);
	}

	
	public void previous(ActionEvent evt)
	{
		Integer idxCurent=this.furnizori.indexOf(furnizor);
		if (idxCurent>0)
			this.furnizor=this.furnizori.get(idxCurent-1);
	}
	public void next(ActionEvent evt)
	{
		Integer idxCurent=this.furnizori.indexOf(furnizor);
		if ((idxCurent+1)<this.furnizori.size())
			this.furnizor=this.furnizori.get(idxCurent+1);
	}
	
	//CRUD
	public void adaugare(ActionEvent evt)
	{
		try {
		this.furnizor=new FurnizorIntern();
		this.furnizor.setId(110);
		this.furnizor.setNume("");
		this.furnizor.setTip("intern");
		this.furnizor.setAdresa("");
		this.furnizor.setTelefon("");
		this.furnizor.setCodFiscal("");
		this.furnizori.add(furnizor);}
		catch (Exception e) {
			alert(e);
		}
		
	}

	public void stergere(ActionEvent evt)
	{
		try {
			this.furnizori.remove(this.furnizor);
			if(this.em.contains(this.furnizor))
			{
				this.em.getTransaction().begin();
				this.em.remove(this.furnizor);
				this.em.getTransaction().commit();
			}
			if(!this.furnizori.isEmpty())
				this.furnizor=this.furnizori.get(0);
			else
				this.furnizor=null;
		} catch (Exception e) {
			alert(e);
		}
	}
	
	private void alert(Exception e) {
		// TODO Auto-generated method stub
		
	}

	public void salvare(ActionEvent evt)
	{
		try {
			this.em.getTransaction().begin();
			this.em.persist(this.furnizor);
			this.em.getTransaction().commit();
	}
		catch  (Exception e) {
			alert(e);
		}}
	public void abandon(ActionEvent evt)
	{
		if(this.em.contains(this.furnizor))
		{ this.em.getTransaction().begin();
		this.em.refresh(this.furnizor);
		this.em.getTransaction().commit();}
		else{ if(this.furnizori.size()>0)
			this.furnizor=this.furnizori.get(0);}
	}

}
