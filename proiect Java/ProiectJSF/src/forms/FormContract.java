package forms;

import java.util.ArrayList;
import java.util.List;

import javax.faces.event.ActionEvent;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

import org.furnizori.model.ContracteAprovizionare;
import org.furnizori.model.Produse;

public class FormContract {

	private ContracteAprovizionare contract;
	private List<ContracteAprovizionare> contracte = new ArrayList<ContracteAprovizionare>();
	
	public ContracteAprovizionare getContract() {
		return contract;
	}
	 public List<ContracteAprovizionare> getContracte() {
		return contracte;}
	 
	
	public void getContractId(ContracteAprovizionare contract) {
		this.contract.getId();
	}
	public void setContractId(Integer id)
	{
		Integer idx=this.contracte.indexOf(new ContracteAprovizionare(id,null,null));
		this.contract=this.contracte.get(idx);
	}

	
	private EntityManager em; 

	public FormContract() {
		EntityManagerFactory emf = Persistence.createEntityManagerFactory("proiectJPA");
		em = emf.createEntityManager();
		this.contracte = em.createQuery("SELECT c FROM ContracteAprovizionare c").getResultList();
		
		System.out.println("contracte.count: " + this.contracte.size());
		
		if (!this.contracte.isEmpty())
			contract = this.contracte.get(0);
	}

	
	public void previous(ActionEvent evt)
	{
		Integer idxCurent=this.contracte.indexOf(contract);
		if (idxCurent>0)
			this.contract=this.contracte.get(idxCurent-1);
	}
	public void next(ActionEvent evt)
	{
		Integer idxCurent=this.contracte.indexOf(contract);
		if ((idxCurent+1)<this.contracte.size())
			this.contract=this.contracte.get(idxCurent+1);
	}
	
	//CRUD
	public void adaugare(ActionEvent evt)
	{
		try {
		this.contract=new ContracteAprovizionare();
		this.contract.setId(1010);
		this.contract.setFurnizor(null);	
		this.contract.setDataContract(null);}
		catch (Exception e) {
			alert(e);
		}
		
	}

	public void stergere(ActionEvent evt)
	{
		try {
			this.contracte.remove(this.contract);
			if(this.em.contains(this.contract))
			{
				this.em.getTransaction().begin();
				this.em.remove(this.contract);
				this.em.getTransaction().commit();
			}
			if(!this.contracte.isEmpty())
				this.contract=this.contracte.get(0);
			else
				this.contract=null;
		} catch (Exception e) {
			alert(e);
		}
	}
	
	private void alert(Exception e) {
		// TODO Auto-generated method stub
		
	}

	public void salvare(ActionEvent evt)
	{
		try {
			this.em.getTransaction().begin();
			this.em.persist(this.contract);
			this.em.getTransaction().commit();
	}
		catch  (Exception e) {
			alert(e);
		}}
	public void abandon(ActionEvent evt)
	{
		if(this.em.contains(this.contract))
		{ this.em.getTransaction().begin();
		this.em.refresh(this.contract);
		this.em.getTransaction().commit();}
		else{ if(this.contracte.size()>0)
			this.contract=this.contracte.get(0);}
	}

	
	
	
}
