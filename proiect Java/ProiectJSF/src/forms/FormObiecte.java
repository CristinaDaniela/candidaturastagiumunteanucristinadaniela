package forms;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.faces.convert.ConverterException;
import javax.faces.event.ActionEvent;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

import org.furnizori.model.*;
public class FormObiecte {

	private ObiecteContract obiect;
	private List<ObiecteContract> obiecte = new ArrayList<ObiecteContract>();
	

	public ObiecteContract getObiect() {
		return obiect;
	}
	public void setObiect(ObiecteContract obiect) {
		this.obiect = obiect;
	}
	public List<ObiecteContract> getObiecte() {
		return obiecte;
	}
	public void setObiecte(List<ObiecteContract> obiecte) {
		this.obiecte = obiecte;
	}
	
	public Integer getIdObiect()
	{
		return this.obiect.getId();
	}
	public void setIdObiect(Integer id)
	{
		Integer idx=this.obiecte.indexOf(new ObiecteContract(id,new Produse(id,"",null,null),null, new ContracteAprovizionare(null,null,null)));
		this.obiect=this.obiecte.get(idx);
	}
	
	
	private EntityManager em; 

	public FormObiecte() {
		EntityManagerFactory emf = Persistence.createEntityManagerFactory("proiectJPA");
		em = emf.createEntityManager();
		this.obiecte = em.createQuery("SELECT o FROM ObiecteContract o").getResultList();
		
		System.out.println("obiecte.count: " + this.obiecte.size());
		
		if (!this.obiecte.isEmpty())
			obiect = this.obiecte.get(0);
	}
	public void previous(ActionEvent evt)
	{
		Integer idxCurent=this.obiecte.indexOf(obiect);
		if (idxCurent>0)
			this.obiect=this.obiecte.get(idxCurent-1);
	}
	public void next(ActionEvent evt)
	{
		Integer idxCurent=this.obiecte.indexOf(obiect);
		if ((idxCurent+1)<this.obiecte.size())
			this.obiect=this.obiecte.get(idxCurent+1);
	}
	
	//CRUD
	public void adaugare(ActionEvent evt)
	{
		ObiecteContract obiect=new ObiecteContract();
		this.obiect.setId(null);
		this.obiect.setProdus(null);
		this.obiect.setCantitate(null);
		this.obiect.setContract(null);
		this.obiecte.add(obiect);
		
	}

	public void stergere(ActionEvent evt)
	{
		this.obiecte.remove(this.obiect);
		if(this.em.contains(this.obiect))
		{
			this.em.getTransaction().begin();
			this.em.remove(this.obiect);
			this.em.getTransaction().commit();
		}
		if(!this.obiecte.isEmpty())
			this.obiect=this.obiecte.get(0);
		else
			this.obiect=null;
	}
	
	public void salvare(ActionEvent evt)
	{
		this.em.getTransaction().begin();
		this.em.persist(this.obiect);
		this.em.getTransaction().commit();
	}
	public void abandon(ActionEvent evt)
	{
	
		if(this.em.contains(this.obiect))
			this.em.refresh(this.obiect);
	}
}
	
